const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
// const ImageminWebpackPlugin = require("imagemin-webpack-plugin").default;
// const ImageminWebP = require("imagemin-webp");
const CopyPlugin = require('copy-webpack-plugin');
module.exports = {
    entry: {
        'index': './src/index.js'     
    },
    output: {
        filename: '[name].[contenthash].js',
        path: path.resolve(__dirname, './dist'),
        publicPath: ''
    },
    mode: 'production',
    optimization:{
        splitChunks: {
            chunks: "all",
            minSize: 10000,
            automaticNameDelimiter: '_'
        }
    },
    module: {
        rules: [
            {
                test: /\.(png|jpe?g|gif|svg|mov|mp4)$/i,
                loader: 'file-loader',
                options: {
                  name: 'assets/[name].[ext]',
                },
            },
            {
                test: '/\.css$/',
                use: [
                    MiniCssExtractPlugin.loader, 'css-loader'
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'
                ]
            },
            {
                test: '/\.js$/',
                exclude: '/node_modules/',
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/env'],
                    }
                }
            }
            
        ]
    },
    plugins: [
        //   new CopyPlugin({
        //     patterns: [
        //       { from: "./src/images/*.png", to: "./images/[name].webp" }
        //     ],
        //   }),
        new MiniCssExtractPlugin({
            filename: '[name].[contenthash].css'
        }),
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin(
            {
                filename: 'index.html',
                chunks: ['index'],
                template: 'index.html',
                meta: {
                    description: "something descriptive"
                }
            }
        )
    ],
}
//TODO: Add common vendors file to every file
